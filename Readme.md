# Dotfiles Management 

__What__: the set of customized configuration files of my working environment

__Why__: backup and replication across computers and through installations

__How__: [git](https://git-scm.com/) + [GNU Stow](https://www.gnu.org/software/stow/) + [GNU Make](https://www.gnu.org/software/make/)

I have always been reluctant to over-customize my working environment since it was something that was lost each time I switched machines or started off a freshly-installed system.
This makes replicating the environment a matter of seconds.

Moreover, it can efficiently handle computer-specific configurations.

Thanks to [Jeebak Kim](https://github.com/jeebak/dotmatrix) from whom I borrowed the first version of the awesome `Makefile`


## Requirements/Installation:

[GNU Stow](https://www.gnu.org/software/stow/)

```bash
sudo apt-get install stow
git clone git@gitlab.com:famuvie/dotfiles.git ~/.dotfiles
## git checkout <branch>  # if necessary, see below
```

[Understand how it works](http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html) (it's very easy for a simple approach)

The _stow_ dir `~/.dotfiles` can actually be located anywhere, since the `Makefile` always explicitly defines the target directory as `$HOME`.

For computer-specific environments, checkout the corresponding branch (e.g. `git checkout fmhm`).


## Usage

The _"magic's"_ in the `Makefile`. 
The default action is to `stow`. 
The action is applied to a list of stowable __targets__ (the package folders).

Examples:
```bash
make             # defaults to: stow-all target/action
make bash
make vim zsh
## git difftool  # review adopted files
```

There's also a little bit of
[Make](https://www.gnu.org/software/make/manual/html_node/Text-Functions.html)
trickery to allow you to specfify `unstow` or `restow` as actions. These can
only be specfied as the 1st argument. Additionally, there are `stow-all`,
`unstow-all`, `restow-all` targets.

Examples:
```bash
make unstow bash vim zsh
make restow bash vim zsh

make stow-all    # default action
make unstow-all  # remove links
make restow-all  # remove and re-link 
```

## Conflicts

- If a target link already exists, `stow` will not modify it. Use `restow` if necessary.

- If a target _hard_ file already exists, `stow` will __adopt__ it. 
  I.e., the file will overwrite the current version in the __stow__ repository. 
  Since the repo is under version control, it's easy to spot the changes (e.g., `git difftool`), take or leave the desired changes, and then __restow__ some or all of the targets


## Dotfile examples


- https://dotfiles.github.io/
